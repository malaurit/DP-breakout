# Displayport breakout board
This repository contains Kicad files for a Displayport to pin header breakout board.
![](https://i.imgur.com/0DMc298.jpg)

The folder /Gerber contains all production files required. Simply send these to a manufacturer and you're off. 

#### One breakout board requires the following components:
- 1 Displayport connector, Molex 0472720001: https://www.digikey.com/product-detail/en/molex-llc/0472720001/WM19271CT-ND/1658341
- 4 single pin headers for ground.
- 4 double pin headers for signals.
- 4 angled pin headers for signals.
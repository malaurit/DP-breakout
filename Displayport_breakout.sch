EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:Magnes Components
LIBS:Displayport_breakout-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Displayport breakout board"
Date ""
Rev "A"
Comp "University of Bergen"
Comment1 "Magne Lauritzen"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L R R1
U 1 1 5B0EC684
P 5850 2900
F 0 "R1" V 5930 2900 50  0000 C CNN
F 1 "100" V 5850 2900 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 5780 2900 50  0001 C CNN
F 3 "" H 5850 2900 50  0001 C CNN
	1    5850 2900
	1    0    0    -1  
$EndComp
$Comp
L R R2
U 1 1 5B0EC9DF
P 5850 3350
F 0 "R2" V 5930 3350 50  0000 C CNN
F 1 "100" V 5850 3350 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 5780 3350 50  0001 C CNN
F 3 "" H 5850 3350 50  0001 C CNN
	1    5850 3350
	1    0    0    -1  
$EndComp
$Comp
L R R3
U 1 1 5B0ECA59
P 5850 3800
F 0 "R3" V 5930 3800 50  0000 C CNN
F 1 "100" V 5850 3800 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 5780 3800 50  0001 C CNN
F 3 "" H 5850 3800 50  0001 C CNN
	1    5850 3800
	1    0    0    -1  
$EndComp
$Comp
L R R4
U 1 1 5B0ECA65
P 5850 4250
F 0 "R4" V 5930 4250 50  0000 C CNN
F 1 "100" V 5850 4250 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 5780 4250 50  0001 C CNN
F 3 "" H 5850 4250 50  0001 C CNN
	1    5850 4250
	1    0    0    -1  
$EndComp
$Comp
L R R5
U 1 1 5B0ECBC6
P 5850 4750
F 0 "R5" V 5930 4750 50  0000 C CNN
F 1 "100" V 5850 4750 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 5780 4750 50  0001 C CNN
F 3 "" H 5850 4750 50  0001 C CNN
	1    5850 4750
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x02_Male J1
U 1 1 5B0FFF5D
P 6300 2850
F 0 "J1" H 6300 2950 50  0000 C CNN
F 1 "Conn_01x02_Male" H 6300 2650 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Angled_1x02_Pitch2.54mm" H 6300 2850 50  0001 C CNN
F 3 "" H 6300 2850 50  0001 C CNN
	1    6300 2850
	-1   0    0    1   
$EndComp
$Comp
L Conn_01x02_Male J6
U 1 1 5B10003C
P 6600 2850
F 0 "J6" H 6600 2950 50  0000 C CNN
F 1 "Conn_01x02_Male" H 6600 2550 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 6600 2850 50  0001 C CNN
F 3 "" H 6600 2850 50  0001 C CNN
	1    6600 2850
	-1   0    0    1   
$EndComp
$Comp
L Conn_01x02_Male J5
U 1 1 5B1003FD
P 6300 4700
F 0 "J5" H 6300 4800 50  0000 C CNN
F 1 "Conn_01x02_Male" H 6300 4500 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Angled_1x02_Pitch2.54mm" H 6300 4700 50  0001 C CNN
F 3 "" H 6300 4700 50  0001 C CNN
	1    6300 4700
	-1   0    0    1   
$EndComp
$Comp
L Conn_01x02_Male J10
U 1 1 5B100403
P 6600 4700
F 0 "J10" H 6600 4800 50  0000 C CNN
F 1 "Conn_01x02_Male" H 6600 4400 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 6600 4700 50  0001 C CNN
F 3 "" H 6600 4700 50  0001 C CNN
	1    6600 4700
	-1   0    0    1   
$EndComp
$Comp
L Conn_01x02_Male J3
U 1 1 5B10047F
P 6300 3750
F 0 "J3" H 6300 3850 50  0000 C CNN
F 1 "Conn_01x02_Male" H 6300 3550 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Angled_1x02_Pitch2.54mm" H 6300 3750 50  0001 C CNN
F 3 "" H 6300 3750 50  0001 C CNN
	1    6300 3750
	-1   0    0    1   
$EndComp
$Comp
L Conn_01x02_Male J8
U 1 1 5B100485
P 6600 3750
F 0 "J8" H 6600 3850 50  0000 C CNN
F 1 "Conn_01x02_Male" H 6600 3450 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 6600 3750 50  0001 C CNN
F 3 "" H 6600 3750 50  0001 C CNN
	1    6600 3750
	-1   0    0    1   
$EndComp
$Comp
L Conn_01x02_Male J4
U 1 1 5B10054F
P 6300 4200
F 0 "J4" H 6300 4300 50  0000 C CNN
F 1 "Conn_01x02_Male" H 6300 4000 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Angled_1x02_Pitch2.54mm" H 6300 4200 50  0001 C CNN
F 3 "" H 6300 4200 50  0001 C CNN
	1    6300 4200
	-1   0    0    1   
$EndComp
$Comp
L Conn_01x02_Male J9
U 1 1 5B100555
P 6600 4200
F 0 "J9" H 6600 4300 50  0000 C CNN
F 1 "Conn_01x02_Male" H 6600 3900 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 6600 4200 50  0001 C CNN
F 3 "" H 6600 4200 50  0001 C CNN
	1    6600 4200
	-1   0    0    1   
$EndComp
$Comp
L Conn_01x02_Male J2
U 1 1 5B10069C
P 6300 3300
F 0 "J2" H 6300 3400 50  0000 C CNN
F 1 "Conn_01x02_Male" H 6300 3100 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Angled_1x02_Pitch2.54mm" H 6300 3300 50  0001 C CNN
F 3 "" H 6300 3300 50  0001 C CNN
	1    6300 3300
	-1   0    0    1   
$EndComp
$Comp
L Conn_01x02_Male J7
U 1 1 5B1006A2
P 6600 3300
F 0 "J7" H 6600 3400 50  0000 C CNN
F 1 "Conn_01x02_Male" H 6600 3000 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 6600 3300 50  0001 C CNN
F 3 "" H 6600 3300 50  0001 C CNN
	1    6600 3300
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR01
U 1 1 5B100796
P 5100 5600
F 0 "#PWR01" H 5100 5350 50  0001 C CNN
F 1 "GND" H 5100 5450 50  0000 C CNN
F 2 "" H 5100 5600 50  0001 C CNN
F 3 "" H 5100 5600 50  0001 C CNN
	1    5100 5600
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x01 J11
U 1 1 5B1110EA
P 5500 5050
F 0 "J11" H 5500 5150 50  0000 C CNN
F 1 "Conn_01x01" H 5500 4950 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x01_Pitch2.54mm" H 5500 5050 50  0001 C CNN
F 3 "" H 5500 5050 50  0001 C CNN
	1    5500 5050
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x01 J12
U 1 1 5B111723
P 5500 5300
F 0 "J12" H 5500 5400 50  0000 C CNN
F 1 "Conn_01x01" H 5500 5200 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x01_Pitch2.54mm" H 5500 5300 50  0001 C CNN
F 3 "" H 5500 5300 50  0001 C CNN
	1    5500 5300
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x01 J13
U 1 1 5B1117B4
P 5500 5550
F 0 "J13" H 5500 5650 50  0000 C CNN
F 1 "Conn_01x01" H 5500 5450 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x01_Pitch2.54mm" H 5500 5550 50  0001 C CNN
F 3 "" H 5500 5550 50  0001 C CNN
	1    5500 5550
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x01 J14
U 1 1 5B11185B
P 5950 5050
F 0 "J14" H 5950 5150 50  0000 C CNN
F 1 "Conn_01x01" H 5950 4950 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x01_Pitch2.54mm" H 5950 5050 50  0001 C CNN
F 3 "" H 5950 5050 50  0001 C CNN
	1    5950 5050
	1    0    0    -1  
$EndComp
$Comp
L TEST TP1
U 1 1 5B111971
P 6800 2750
F 0 "TP1" H 6800 3050 50  0000 C BNN
F 1 "TEST" H 6800 3000 50  0000 C CNN
F 2 "Measurement_Points:Measurement_Point_Round-SMD-Pad_Big" H 6800 2750 50  0001 C CNN
F 3 "" H 6800 2750 50  0001 C CNN
	1    6800 2750
	1    0    0    -1  
$EndComp
$Comp
L TEST TP2
U 1 1 5B111B4E
P 6950 2850
F 0 "TP2" H 6950 3150 50  0000 C BNN
F 1 "TEST" H 6950 3100 50  0000 C CNN
F 2 "Measurement_Points:Measurement_Point_Round-SMD-Pad_Big" H 6950 2850 50  0001 C CNN
F 3 "" H 6950 2850 50  0001 C CNN
	1    6950 2850
	1    0    0    -1  
$EndComp
$Comp
L TEST TP3
U 1 1 5B111E23
P 6750 3200
F 0 "TP3" H 6750 3500 50  0000 C BNN
F 1 "TEST" H 6750 3450 50  0000 C CNN
F 2 "Measurement_Points:Measurement_Point_Round-SMD-Pad_Big" H 6750 3200 50  0001 C CNN
F 3 "" H 6750 3200 50  0001 C CNN
	1    6750 3200
	1    0    0    -1  
$EndComp
$Comp
L TEST TP7
U 1 1 5B111E29
P 6900 3300
F 0 "TP7" H 6900 3600 50  0000 C BNN
F 1 "TEST" H 6900 3550 50  0000 C CNN
F 2 "Measurement_Points:Measurement_Point_Round-SMD-Pad_Big" H 6900 3300 50  0001 C CNN
F 3 "" H 6900 3300 50  0001 C CNN
	1    6900 3300
	1    0    0    -1  
$EndComp
$Comp
L TEST TP4
U 1 1 5B111E93
P 6750 3650
F 0 "TP4" H 6750 3950 50  0000 C BNN
F 1 "TEST" H 6750 3900 50  0000 C CNN
F 2 "Measurement_Points:Measurement_Point_Round-SMD-Pad_Big" H 6750 3650 50  0001 C CNN
F 3 "" H 6750 3650 50  0001 C CNN
	1    6750 3650
	1    0    0    -1  
$EndComp
$Comp
L TEST TP8
U 1 1 5B111E99
P 6900 3750
F 0 "TP8" H 6900 4050 50  0000 C BNN
F 1 "TEST" H 6900 4000 50  0000 C CNN
F 2 "Measurement_Points:Measurement_Point_Round-SMD-Pad_Big" H 6900 3750 50  0001 C CNN
F 3 "" H 6900 3750 50  0001 C CNN
	1    6900 3750
	1    0    0    -1  
$EndComp
$Comp
L TEST TP5
U 1 1 5B111F6F
P 6750 4100
F 0 "TP5" H 6750 4400 50  0000 C BNN
F 1 "TEST" H 6750 4350 50  0000 C CNN
F 2 "Measurement_Points:Measurement_Point_Round-SMD-Pad_Big" H 6750 4100 50  0001 C CNN
F 3 "" H 6750 4100 50  0001 C CNN
	1    6750 4100
	1    0    0    -1  
$EndComp
$Comp
L TEST TP9
U 1 1 5B111F75
P 6900 4200
F 0 "TP9" H 6900 4500 50  0000 C BNN
F 1 "TEST" H 6900 4450 50  0000 C CNN
F 2 "Measurement_Points:Measurement_Point_Round-SMD-Pad_Big" H 6900 4200 50  0001 C CNN
F 3 "" H 6900 4200 50  0001 C CNN
	1    6900 4200
	1    0    0    -1  
$EndComp
$Comp
L TEST TP6
U 1 1 5B111FE7
P 6750 4600
F 0 "TP6" H 6750 4900 50  0000 C BNN
F 1 "TEST" H 6750 4850 50  0000 C CNN
F 2 "Measurement_Points:Measurement_Point_Round-SMD-Pad_Big" H 6750 4600 50  0001 C CNN
F 3 "" H 6750 4600 50  0001 C CNN
	1    6750 4600
	1    0    0    -1  
$EndComp
$Comp
L TEST TP10
U 1 1 5B111FED
P 6900 4700
F 0 "TP10" H 6900 5000 50  0000 C BNN
F 1 "TEST" H 6900 4950 50  0000 C CNN
F 2 "Measurement_Points:Measurement_Point_Round-SMD-Pad_Big" H 6900 4700 50  0001 C CNN
F 3 "" H 6900 4700 50  0001 C CNN
	1    6900 4700
	1    0    0    -1  
$EndComp
Text Label 5250 2750 0    60   ~ 0
Lane0+
Text Label 5250 3100 0    60   ~ 0
Lane0-
Text Label 5250 3200 0    60   ~ 0
Lane1+
Text Label 5250 3400 0    60   ~ 0
Lane1-
Text Label 5250 3500 0    60   ~ 0
Lane2+
Text Label 5250 3700 0    60   ~ 0
Lane2-
Text Label 5250 3800 0    60   ~ 0
Lane3+
Text Label 5250 4000 0    60   ~ 0
Lane3-
Text Label 5250 4300 0    60   ~ 0
Aux+
Text Label 5250 4500 0    60   ~ 0
Aux-
$Comp
L 0472720001 U1
U 1 1 5B115665
P 4550 3700
F 0 "U1" H 5000 4650 60  0000 C CNN
F 1 "0472720001" H 4350 4650 60  0000 C CNN
F 2 "Connectors_custom:DISPLAYPORT" H 4900 4200 60  0001 C CNN
F 3 "" H 4900 4200 60  0001 C CNN
	1    4550 3700
	1    0    0    -1  
$EndComp
Connection ~ 5100 4900
Wire Wire Line
	5000 4900 5100 4900
Wire Wire Line
	5000 2900 5250 2900
Connection ~ 6400 4700
Connection ~ 6400 4600
Connection ~ 6400 4200
Connection ~ 6400 4100
Connection ~ 6400 3750
Connection ~ 6400 3650
Connection ~ 6400 3300
Connection ~ 6400 3200
Connection ~ 5300 5050
Connection ~ 5100 5050
Connection ~ 5300 5300
Wire Wire Line
	5300 5050 5300 5550
Wire Wire Line
	5100 5050 5750 5050
Wire Wire Line
	6100 2850 6950 2850
Wire Wire Line
	5250 2750 6800 2750
Connection ~ 6400 2850
Connection ~ 6400 2750
Connection ~ 5100 3300
Wire Wire Line
	5100 3000 5000 3000
Connection ~ 5100 3600
Wire Wire Line
	5100 3300 5000 3300
Connection ~ 5100 3900
Wire Wire Line
	5100 3600 5000 3600
Connection ~ 5100 4400
Wire Wire Line
	5100 3900 5000 3900
Wire Wire Line
	5100 4400 5000 4400
Wire Wire Line
	5100 3000 5100 5600
Wire Wire Line
	6100 3300 6900 3300
Connection ~ 6100 3200
Wire Wire Line
	6100 4200 6900 4200
Connection ~ 6100 4100
Wire Wire Line
	6100 3750 6900 3750
Connection ~ 6100 3650
Wire Wire Line
	6100 4700 6900 4700
Connection ~ 6100 4600
Wire Wire Line
	6100 3100 6100 2850
Connection ~ 6100 2750
Connection ~ 5850 4900
Wire Wire Line
	5350 4500 5350 4900
Wire Wire Line
	5000 4500 5350 4500
Connection ~ 5850 4600
Wire Wire Line
	5400 4300 5000 4300
Wire Wire Line
	5400 4600 5400 4300
Connection ~ 5850 4400
Wire Wire Line
	5500 4000 5000 4000
Wire Wire Line
	5500 4400 5500 4000
Connection ~ 5850 4100
Wire Wire Line
	5550 3800 5000 3800
Wire Wire Line
	5550 4100 5550 3800
Connection ~ 5850 3650
Wire Wire Line
	5650 3500 5000 3500
Wire Wire Line
	5650 3650 5650 3500
Wire Wire Line
	5000 3700 5650 3700
Connection ~ 5850 3950
Wire Wire Line
	5650 3700 5650 3950
Connection ~ 5850 3100
Wire Wire Line
	5850 3050 5850 3100
Wire Wire Line
	5000 3100 6100 3100
Wire Wire Line
	5350 4900 6100 4900
Wire Wire Line
	6100 4900 6100 4700
Wire Wire Line
	5650 3950 6100 3950
Wire Wire Line
	6100 3950 6100 3750
Wire Wire Line
	5500 4400 6100 4400
Wire Wire Line
	6100 4400 6100 4200
Wire Wire Line
	5400 4600 6750 4600
Wire Wire Line
	5550 4100 6750 4100
Wire Wire Line
	5650 3650 6750 3650
Connection ~ 5850 3500
Wire Wire Line
	6100 3500 6100 3300
Wire Wire Line
	5750 3400 5000 3400
Wire Wire Line
	5750 3500 5750 3400
Wire Wire Line
	5750 3500 6100 3500
Connection ~ 5850 3200
Wire Wire Line
	5000 3200 6750 3200
Connection ~ 5850 2750
Wire Wire Line
	5250 2900 5250 2750
$EndSCHEMATC
